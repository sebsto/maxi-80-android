package com.stormacq.android.maxi80;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

public class CollectUsageStatistics {
	
	private static CollectUsageStatistics singleton = null;
	private CollectUsageStatistics() {}
	
	private GoogleAnalyticsTracker tracker;
	
	public static CollectUsageStatistics getSingleton(Activity activity) {
		
		synchronized(CollectUsageStatistics.class) {
			if (singleton == null) {
					singleton = new CollectUsageStatistics(); 
					singleton.initGAN(activity);
			}
		}
		

		return singleton;
	}
	
	public void recordStartofUse(Activity activity) {
		collectData(activity, true);
	}
	
	public void recordEndOfUse(Activity activity) {
		collectData(activity, false);
	}
	
	private void collectData(Activity activity, boolean start) {
		GoogleAnalyticsTracker tracker = GoogleAnalyticsTracker.getInstance();
		tracker.trackEvent("radio", start ? "start" : "stop", "", -1);
	}
	
	public void stopTracking() {
		tracker.stopSession();
		
	}
	
	private void initGAN(Activity activity) {
		TelephonyManager tm = (TelephonyManager)activity.getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
		tracker = GoogleAnalyticsTracker.getInstance();
		tracker.setAnonymizeIp(false);

	    // Start the tracker in automatic dispatch mode...
	    tracker.startNewSession("UA-11036224-2", 10, activity);
	    
	    tracker.setCustomVar(1, "deviceName", tm.getDeviceId());
	    tracker.setCustomVar(2, "deviceSystemName", "Android");
	    tracker.setCustomVar(3, "deviceSystemVersion", Build.VERSION.RELEASE);
	    tracker.setCustomVar(4, "deviceModel", Build.MANUFACTURER + " " + Build.MODEL);
	}
}
